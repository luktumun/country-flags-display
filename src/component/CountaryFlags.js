import "./CountaryFlags.css";

import React, { useEffect, useState } from "react";

const CountaryFlags = () => {
  const [data, setData] = useState(null);

  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((response) => response.json())
      .then((data) => {
        setData(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <>
      {data &&
        data.map((item) => (
          <li key={item.name.common}>
            <img src={item.flags.png} alt={item.flags.alt} />

            <p>{item.name.common}</p>
          </li>
        ))}
    </>
  );
};

export default CountaryFlags;
